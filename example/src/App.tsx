/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, { useState } from 'react';
import {
  SafeAreaView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
} from 'react-native';

import type LottieView from 'lottie-react-native';
import { Colors } from 'react-native/Libraries/NewAppScreen';
import { ScanCode } from 'rn-barcode-zxing';

const App = () => {
  const ref = React.useRef<LottieView>(null);
  const isDarkMode = useColorScheme() === 'dark';

  const [code, setCode] = useState('');

  const backgroundStyle = {
    backgroundColor: isDarkMode ? Colors.darker : Colors.lighter,
  };

  return (
    <SafeAreaView style={[{ flex: 1 }, backgroundStyle]}>
      <StatusBar barStyle={isDarkMode ? 'light-content' : 'dark-content'} />
      <View style={{ flex: 1 }}>
        <ScanCode
          shouldScan={true}
          onScanBarcode={(results) => {
            setCode(JSON.stringify(results));
          }}
          markProps={{
            ref,
            style: { width: '100%', height: '100%' },
          }}
        />
        <View style={styles.results}>
          <Text style={styles.code}>{code}</Text>
        </View>
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  sectionContainer: {
    marginTop: 32,
    paddingHorizontal: 24,
  },
  sectionTitle: {
    fontSize: 24,
    fontWeight: '600',
  },
  sectionDescription: {
    marginTop: 8,
    fontSize: 18,
    fontWeight: '400',
  },
  highlight: {
    fontWeight: '700',
  },
  results: {
    position: 'absolute',
    top: 100,
    left: 20,
  },
  code: {
    color: '#FFFFFF',
    fontSize: 20,
    fontWeight: '600',
  },
});

export default App;
