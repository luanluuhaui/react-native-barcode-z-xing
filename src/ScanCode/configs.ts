const widthBarCode = 300;
const heightBarCode = 300;
const animatedLineWidth = widthBarCode - 96;
export { widthBarCode, heightBarCode, animatedLineWidth };
