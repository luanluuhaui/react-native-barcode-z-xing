import React, { memo } from 'react';
import {
  Text,
  TouchableOpacity,
  View,
  Image,
  StyleSheet,
  type ViewStyle,
  type ImageStyle,
  type TextStyle,
} from 'react-native';

import styles from './styles';

import ICONS from '../assets/icons';

export type HeaderProps = {
  style?: ViewStyle;
  backButtonStyle?: ViewStyle;
  iconBackStyle?: ImageStyle;
  headerTextStyle?: TextStyle;
  headerText?: String;

  visibleHeaderLeft?: boolean;
  visibleHeaderRight?: boolean;
  HeaderLeft?: React.ReactNode | React.Component | Function;
  HeaderRight?: React.ReactNode | React.Component | Function;

  onPressLeftButton?: (() => void) | undefined;
};

const HeaderScreen = (props: HeaderProps) => {
  const { HeaderRight, HeaderLeft, onPressLeftButton } = props;

  const renderHeaderLeft = () => {
    if (props.visibleHeaderLeft !== false && !HeaderLeft) {
      return (
        <TouchableOpacity
          onPress={onPressLeftButton}
          style={[styles.buttonMenu, StyleSheet.flatten(props.backButtonStyle)]}
        >
          <Image
            source={ICONS.icon_back}
            style={[
              styles.iconButtonMenu,
              StyleSheet.flatten(props.iconBackStyle),
            ]}
          />
        </TouchableOpacity>
      );
    }
    if (typeof HeaderLeft === 'function') {
      return HeaderLeft();
    }
    return HeaderLeft;
  };

  const renderHeaderRight = () => {
    if (!HeaderRight) return null;
    if (typeof HeaderRight === 'function') {
      return HeaderRight();
    }
    return HeaderRight;
  };

  return (
    <View style={[styles.header, StyleSheet.flatten(props.style)]}>
      {renderHeaderLeft()}
      <Text
        style={[
          styles.textScreenName,
          StyleSheet.flatten(props.headerTextStyle),
        ]}
      >
        {props.headerText || 'Scanner'}
      </Text>
      {renderHeaderRight()}
    </View>
  );
};

export default memo(HeaderScreen);
