import { StyleSheet, Dimensions } from 'react-native';

const { width } = Dimensions.get('window');
const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  content: {
    flex: 1,
  },
  header: {
    width,
    height: 44,
    backgroundColor: '#FEFEFE',
    alignItems: 'center',
    flexDirection: 'row',
  },
  buttonMenu: {
    width: 36,
    height: 36,
    alignItems: 'center',
    justifyContent: 'center',
  },
  iconButtonMenu: {
    width: 20,
    height: 20,
  },
  textScreenName: {
    fontSize: 18,
    color: '#212121',
    paddingRight: 15,
    textAlign: 'center',
  },
  cameraStyle: {
    flex: 1,
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
  },
});

export default styles;
