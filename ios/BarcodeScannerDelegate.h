//
//  BarcodeScannerDelegate.h
//  RNBarcodeZXing
//
//  Created by LUANLUU on 10/12/2021.
//  Copyright © 2021 Facebook. All rights reserved.
//

#ifndef BarcodeScannerDelegate_h
#define BarcodeScannerDelegate_h

#endif /* BarcodeScannerDelegate_h */

#import "BarcodeScannerView.h"

@protocol BarcodeScannerDelegate <NSObject>
- (void)onRead:(UIView *)barcodeView result:(NSDictionary*)result;
@end
