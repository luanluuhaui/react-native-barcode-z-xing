//
//  RNBarcodeScanner.m
//  RNBarcodeZXing
//
//  Created by LUANLUU on 14/05/2021.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
#import <React/RCTViewManager.h>

#import "BarcodeScannerView.h"

@interface BarcodeScannerManager : RCTViewManager <BarcodeScannerDelegate>
@end

@implementation BarcodeScannerManager

RCT_EXPORT_MODULE(BarcodeScanner)

RCT_EXPORT_VIEW_PROPERTY(onBarcodesDetected, RCTBubblingEventBlock)

- (UIView *)view
{
  BarcodeScannerView *bsv = [BarcodeScannerView new];
  bsv.delegate = self;
  return bsv;
}

- (void)onRead:(BarcodeScannerView *)barcodeView result:(NSDictionary *)result {
  if(!barcodeView.onBarcodesDetected){
    return;
  }
  barcodeView.onBarcodesDetected(result);
}

@end
