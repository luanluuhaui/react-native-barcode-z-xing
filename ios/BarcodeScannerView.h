//
//  BarcodeScannerView.h
//  RNBarcodeZXing
//
//  Created by LUANLUU on 07/06/2021.
//  Copyright © 2021 Facebook. All rights reserved.
//

#ifndef BarcodeScannerView_h
#define BarcodeScannerView_h


#endif /* BarcodeScannerView_h */

#import <UIKit/UIKit.h>
#import <React/RCTComponent.h>
#import <ZXingObjC/ZXingObjC.h>

#import "BarcodeScannerDelegate.h"

@interface BarcodeScannerView: UIView <ZXCaptureDelegate>

@property (nonatomic, weak) id<BarcodeScannerDelegate> delegate;
@property (nonatomic, copy) RCTBubblingEventBlock onBarcodesDetected;
@property (nonatomic, strong) ZXCapture *capture;
@property (nonatomic) BOOL scanning;
@property (nonatomic) BOOL isFirstApplyOrientation;

@end



