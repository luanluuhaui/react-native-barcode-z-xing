package com.rnbarcodezxing;

import androidx.annotation.NonNull;

import com.facebook.react.bridge.Promise;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.module.annotations.ReactModule;

@ReactModule(name = BarcodeZXingModule.NAME)
public class BarcodeZXingModule extends ReactContextBaseJavaModule {
  public static final String NAME = "BarcodeZXing";

  public BarcodeZXingModule(ReactApplicationContext reactContext) {
    super(reactContext);
  }

  @Override
  @NonNull
  public String getName() {
    return NAME;
  }
}
